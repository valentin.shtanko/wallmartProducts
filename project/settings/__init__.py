from .base import *

try:
    from .dev import *
except ImportError as e:
    from .production import *
    print(e)
