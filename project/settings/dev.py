"""
Developer settings for vacation project.

"""
DEBUG = True

TEST_MODE = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'wpd',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    }
}
import djcelery, os

os.environ["CELERY_LOADER"] = "django"
djcelery.setup_loader()
AMQP_HOST = 'localhost'
BROKER_HOST='localhost'
BROKER_PORT = 5672
BROKER_VHOST = "/"
BROKER_USER = "myuser"
BROKER_PASSWORD = "mypassword"
