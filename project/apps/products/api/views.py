import json
from urllib.parse import urlparse

from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt
from rest_framework import permissions
from rest_framework.generics import ListAPIView, ListCreateAPIView

from django.http import HttpResponse, HttpResponseNotFound
from rest_framework.views import APIView

from project.apps.products.models import WalmartProduct
from project.apps.products.api.serializer import ProductsSerializer
from project.apps.products.walmart_api.core import WalmartApiClient

class ProductsApiView(APIView):

    serializer_class = ProductsSerializer

    def filter_queryset(self, queryset):
        if not self.request.user.is_superuser:
            self.unfiltered_query_set = queryset = queryset.filter(user=self.request.user)
        return queryset

    def get(self, request, format=None):
        params = request.GET
        limit = 10
        offset = 0
        if (params.get('offset', '')):
            offset = int(params.get('offset', ''))
            if (params.get('limit', '')):
                limit = int(params.get('limit', '')) + offset

        quantity = params.get('quantity', '')
        price = params.get('price', '')
        lastUpdate = params.get('lastUpdate', '')

        response = {
            'sEcho': '',
            'aaData': [],
            'iTotalRecords': 0,
            'iTotalDisplayRecords': 0,
        }

        products = WalmartProduct.objects.filter(user_id=request.user.id)
        products_count = products.count()
        if(quantity):
            products = products.filter(qty__istartswith = quantity)
        if(price):
            products = products.filter(price__istartswith = price)

        products = products[offset:limit]
        for product in products:
            response['aaData'].append({
                'id': str(product.id),
                'image_url': str(product.image),
                'upc': str(product.upc),
                'name': str(product.name),
                'brand': str(product.brand),
                'model': str(product.model),
                'price': str(product.price),
                'in_stock': product.in_stock,
                'free_shipping': product.free_shipping,
                'last_update': str(product.last_update)
            })
        response['iTotalRecords'] = products_count
        response['iTotalDisplayRecords'] = products.count()
        return HttpResponse(json.dumps(response), content_type='application/json')

    def post(self, request):
        params = request.post
        return HttpResponse(json.dumps({'hello':'world'}), content_type='application/json')


def check_upc_with_api(request):
    upc = request.GET.get('upc')
    client = WalmartApiClient('4hg4zzebppgj6gpfhatx6qcj')
    api_response = client.call('items',{'upc':upc}).get('items').pop()
    main_image_url = urlparse(api_response.get('thumbnailImage'))
    imageUrl = main_image_url.scheme + "://" + main_image_url.netloc + main_image_url.path
    response = {
        'name': api_response.get('name'),
        'brand': api_response.get('brandName'),
        'model': api_response.get('modelNumber'),
        'imgUrl': imageUrl,
        'productUrl': api_response.get('productUrl')
    }
    return HttpResponse(json.dumps(response), content_type='application/json')

def upload_product_via_upc(request):
    upc = request.GET.get('upc')
    client = WalmartApiClient('4hg4zzebppgj6gpfhatx6qcj')
    jsonFields = client.call('items', {'upc': upc}).get('items').pop()

    parsed_url = urlparse(jsonFields.get('thumbnailImage'))
    imageUrl = parsed_url.scheme + "://" + parsed_url.netloc + parsed_url.path
    in_stock = True if (jsonFields.get('stock') == 'Available') else False
    free_shipping = True if jsonFields.get('freeShipToStore') else False

    WalmartProduct.objects.update_or_create(
        upc = jsonFields.get('upc'),
        user_id = request.user.id,
        name = jsonFields.get('name'),
        brand = jsonFields.get('brandName'),
        model = jsonFields.get('modelNumber'),
        in_stock = in_stock,
        price = jsonFields.get('salePrice'),
        image = imageUrl,
        free_shipping = free_shipping
    )
    return HttpResponse(json.dumps(jsonFields), content_type='application/json')

@csrf_exempt
def delete_product_by_id(request):
    product_id = json.loads(request.body.decode('utf-8'))['id']
    s = WalmartProduct.objects.filter(id=product_id, user_id=request.user.id).delete()
    return HttpResponse(json.dumps(s), content_type='application/json')

@csrf_exempt
def upload_file_with_upc(request):
    if request.method == 'POST':
        csv_file = request.FILES['files']
        fs = FileSystemStorage()
        fs.save('temp_import.csv', csv_file)
        csv_data = fs.open('temp_import.csv', mode='r')
        upc_codes = []
        for record in csv_data:
            if record.rstrip().isnumeric():
                upc_codes.append(record.rstrip())
        fs.delete('temp_import.csv')
    return HttpResponse(json.dumps(upc_codes), content_type='application/json')