from rest_framework import serializers

from project.apps.products.models import WalmartProduct

class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = WalmartProduct
        fields = '__all__'