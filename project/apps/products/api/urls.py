from django.conf.urls import url

from project.apps.products.api.views import ProductsApiView, check_upc_with_api, upload_product_via_upc, delete_product_by_id, upload_file_with_upc

urlpatterns = [
    url(r'^$', ProductsApiView.as_view()),
    url(r'^check',check_upc_with_api ),
    url(r'^create', upload_product_via_upc ),
    url(r'^delete', delete_product_by_id ),
    url(r'^upload', upload_file_with_upc)
]