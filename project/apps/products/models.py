from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from decimal import Decimal

class WalmartProduct(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    upc = models.CharField(max_length=128, default='')
    name = models.CharField(max_length=256, blank=True, default='')
    image = models.CharField(max_length=128, blank=True, default='')
    brand = models.CharField(max_length=128, blank=True, default='')
    model = models.CharField(max_length=128, blank=True, default='')
    price = models.DecimalField(max_digits=19, decimal_places=2, default=Decimal('0.00'))
    in_stock = models.BooleanField(default=False, blank=True)
    free_shipping = models.BooleanField(default=False, blank=True)
    last_update = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        super(WalmartProduct, self).save(*args, **kwargs)
