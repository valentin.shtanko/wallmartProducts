from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from rest_auth.views import LoginView

from rest_framework import generics, permissions
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.exceptions import APIException


from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import UserSerializer


class ExpiredException(APIException):
    status_code = 403
    default_detail = {'non_field_errors':
                          ['Your account has been expired. Please contact info@ezcloudllc.com']
                      }


class CustomLoginView(LoginView):

    def get_response(self):
        if hasattr(self.request.user, 'settings') and self.request.user.settings.expired:
            raise ExpiredException
        return super(CustomLoginView, self).get_response()


class ChangePasswordView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = UserSerializer

    def patch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise APIException(code=403)

        current_password = request.query_params.get('currentPassword', None)
        new_password = request.query_params.get('newPassword', None)
        confirm_password = request.query_params.get('confirmPassword', None)

        # request data validation
        if current_password is None or new_password is None or confirm_password is None:
            raise APIException('Empty fields are not allowed', 400)
        if not request.user.check_password(current_password) or new_password != confirm_password or \
                len(new_password) > 30 or len(confirm_password) > 30:
            raise APIException('Current password is incorrect. Or password > 30 symbols', 400)
        # setting new password
        request.user.set_password(new_password)
        request.user.save()
        login(request, request.user)
        return Response({'detail': 'Password changed successfully'})


class UserList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)
    model = User
    serializer_class = UserSerializer

    def perform_create(self, serializer):
        user = serializer.save()
        user.set_password(user.password)
        user.save()

    def get_queryset(self):
        return User.objects.all().order_by('-pk').exclude(username=self.request.user.username)


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def perform_update(self, serializer):
        user = serializer.save()
        password = self.request.data['password']
        if password != '':
            user.set_password(password)
            user.save()
