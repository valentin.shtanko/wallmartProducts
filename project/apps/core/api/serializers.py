from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):

    exp_date = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'password', 'first_name', 'last_name', 'email', 'username', 'exp_date')
        # write_only_fields = ()
        read_only_fields = ('exp_date',)
        extra_kwargs = {
            'password': {'write_only': True},
        }

    def get_exp_date(self, obj):
        return obj.settings.expiration_date

