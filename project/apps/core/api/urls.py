from django.conf.urls import url, include

from project.apps.aauth.api.views import ChangePasswordView, UserList, UserDetail

users_urls = [
    url(r'^$', UserList.as_view(), name='users-list'),
    url(r'^(?P<pk>\d+)$', UserDetail.as_view(), name='user-detail'),
]

urlpatterns = [
    url(r'^change_password/', ChangePasswordView.as_view()),
    url(r'^users/', include(users_urls))
]
