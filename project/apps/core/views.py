from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import View, TemplateView

# Create your views here.

class AngularApp(TemplateView):
	template_name = 'index.html'

	def get_context_data(self, **kwargs):
		context = super(AngularApp, self).get_context_data(**kwargs)
		context['ANGULAR_URL'] = settings.ANGULAR_URL
		context['NODE_MODULES_URL'] = settings.NODE_MODULES_URL
		context['ADMIN_LTE_URL'] = settings.ADMIN_LTE_URL
		context['GENT_URL'] = settings.NODE_MODULES_URL + 'gentelella/'
		context['Title'] = 'Wallmart Products'
		context['mainColor'] = 'red-light'
		return context