from django.conf.urls import url
from django.contrib.auth.decorators import permission_required


from project.apps.aauth.views import UsersView

urlpatterns = [
    url(r'^$', permission_required('is_staff', raise_exception=True)(UsersView.as_view()), name='users'),
]