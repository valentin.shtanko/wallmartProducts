from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.views.generic import ListView
from django.views.generic.base import TemplateView


class UsersView(LoginRequiredMixin, ListView):

    login_url = '/auth'
    redirect_field_name = None

    template_name = 'users_list.html'
    model = User


class AuthView(TemplateView):

    template_name = 'auth.html'
