"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from project.apps.core.views import AngularApp
from project.apps.aauth.api.views import CustomLoginView
from rest_framework_jwt.views import obtain_jwt_token

api_urls = [
    url(r'^rest-auth/', include('rest_auth.urls', namespace='rest_framework')),
    url(r'^products/', include('project.apps.products.api.urls')),

]

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/',include(api_urls)),
    url(r'^(?!ng/).*$', AngularApp.as_view(), name="angular_app"),
] + static(settings.ANGULAR_URL, document_root=settings.ANGULAR_ROOT)
