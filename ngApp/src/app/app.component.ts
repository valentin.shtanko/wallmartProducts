import {AfterViewInit, Component, OnInit} from '@angular/core';

declare let jQuery: any;
declare let __moduleName: string;

@Component({
  moduleId:  __moduleName,
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent implements OnInit, AfterViewInit {

  ngOnInit() {

  }
  ngAfterViewInit() {

  }
}

