export class Product {
    id: number;
    name: string;
    brand: string;
    model: string;
    price: number;
    qty: number;
    image_url: string;
    in_stock: number;
}
