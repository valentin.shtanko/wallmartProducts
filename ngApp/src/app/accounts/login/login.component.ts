import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../models/user.model';

declare let jQuery: any;
declare let __moduleName: string;

@Component({
    moduleId: __moduleName,
    selector: 'app-login',
    templateUrl: './login.component.html',
    providers: [AuthenticationService]
})
export class LoginComponent implements OnInit {

  bodyClasses: string = 'login-page';
  body = document.getElementsByTagName('body')[0];

  icheck: JQuery;

  model: User;
  loading: boolean;
  returnUrl: string;

  constructor( private authenticationService: AuthenticationService, private router: Router, private route: ActivatedRoute) {
      this.loading = false;
      this.model = new User();
  }

  ngOnInit() {
      if (localStorage.getItem('currentUser')) {
          this.gotoAdmin();
      }
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';

    this.body.classList.add(this.bodyClasses);
    this.icheck = jQuery('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

  }
  login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    if (this.returnUrl.length) {
                        this.router.navigate([this.returnUrl]);
                    } else {
                        this.gotoAdmin();
                    }
                },
                error => {
                    this.loading = false;
                });
  }
  ngOnDestroy() {
    this.body.classList.remove(this.bodyClasses);
  }

  gotoAdmin() {
    this.router.navigate(['/dashboard']);
  }

}
