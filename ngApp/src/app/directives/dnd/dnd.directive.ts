import {Directive, HostListener, HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[appDnd]'
})
export class DndDirective {

  @HostBinding('style.background') private background = '#eee';
  @Input() files: any = [];
  @Input() counter: number;

  // public files: any = [];
  constructor() { }

  @HostListener('dragover', ['$event']) public onDragOver(evt: any) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#999';
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt: any) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#eee'
  }

  @HostListener('drop', ['$event']) public onDrop(evt: any) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#eee';

    if (evt.dataTransfer.files.length > 0) {
      for (let file of evt.dataTransfer.files ) {
        if (file.type === 'text/csv') {
          this.files.push(file);
          this.counter = this.files.length;
        }
      }
      console.log(this.files);
    }
  }

}
