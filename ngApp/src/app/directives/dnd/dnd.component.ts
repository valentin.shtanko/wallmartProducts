import {Component, Input, OnInit, Output} from '@angular/core';
import {DndDirective} from "./dnd.directive";

declare let __moduleName: string;
@Component({
  moduleId: __moduleName,
  selector: 'app-dnd',
  templateUrl: './dnd.component.html',
  styleUrls: ['./dnd.component.css'],
  providers: [ DndDirective ],
})
export class DndComponent implements OnInit {
  @Input() fileList: any = [];
  constructor() {
  }

  ngOnInit() {
  }

  remove(index: number) {
    delete(this.fileList[index]);
    this.fileList.length--;
   }
}
