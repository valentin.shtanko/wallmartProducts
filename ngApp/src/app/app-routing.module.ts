import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './accounts/login/login.component';


@NgModule({
  imports: [
    RouterModule.forRoot([
      {  path: '', redirectTo: '/login', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent},
      { path: 'login', component: LoginComponent },
      { path: '**', redirectTo: '/' }

    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
