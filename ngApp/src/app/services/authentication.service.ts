import { Injectable } from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';

import { NgXCookies } from 'ngx-cookies';

import {Observable} from 'rxjs/Observable';

import {User} from '../models/user.model';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class AuthenticationService {
    headers: Headers;
    options: RequestOptions;
    API_URL: string;
    csrftoken: string;
    token: any = {};
    model: User;

    constructor(private http: Http) {
        this.csrftoken = NgXCookies.getCookie('csrftoken');
        this.headers = new Headers();
        this.options = new RequestOptions();
        this.API_URL = '/api/rest-auth';
        this.headers.append('Content-Type', 'application/json');
    }
    request(args: any) {
        let params = args.params || {};
        args = args || {};
        let url = this.API_URL + args.url;
        let method = args.method || 'GET';
        let body = args.body || {};

        if (this.csrftoken && !this.headers.has('X-CSRFToken')) {
            this.headers.append('X-CSRFToken', this.csrftoken);
        }
        if (this.token) {
            this.headers.append('Authorization', 'Token ' + this.token.key);
        }
        this.options.headers = this.headers;
        this.options.url = url;
        this.options.params = params;
        this.options.method = method;
        this.options.body = body;


        return this.http.request(url, this.options );

    }
    profile(): Observable<User> {
        return this.request({
            'method': 'GET',
            'url': '/user/'
        }).map(
            (response: Response) => {
                let userData = response.json();
                let userModel = new User();
                userModel.id = userData.pk;
                userModel.username = userData.username;
                userModel.email = userData.email;
                userModel.firstName = userData.first_name;
                userModel.lastName = userData.last_name;
                localStorage.setItem('currentUser', JSON.stringify(userModel));
                return userModel;
            }
        ).catch(
            (error: any) => {
                return Observable.throw(error);
            }
        );
    }
    login(username: string, password: string) {
        let body =  JSON.stringify({
            username: username,
            password: password
        });
        return this.request({
            'method': 'POST',
            'url': '/login/',
            'body': body
        }).map(
            (response: Response) => {
                this.token = response.json();
                localStorage.setItem('currentUser', JSON.stringify(this.token));
            }
        );
    }
    logout() {
        return this.request({
            'method': 'POST',
            'url': '/logout/'
        }).map(
            (response: Response) => {
                localStorage.removeItem('currentUser');
            }
        );
    }
}