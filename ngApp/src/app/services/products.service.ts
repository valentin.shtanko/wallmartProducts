import { Injectable } from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';

import { NgXCookies } from 'ngx-cookies';

import {Observable} from 'rxjs/Observable';

import {Product} from '../models/product.model';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class ProductsService {
    headers: Headers;
    options: RequestOptions;
    API_URL: string;
    csrftoken: string;
    token: any = {};
    model: Product[];


    constructor(private http: Http) {
        this.csrftoken = NgXCookies.getCookie('csrftoken');
        this.headers = new Headers();
        this.options = new RequestOptions();
        this.API_URL = '/api/products/';
        this.headers.append('Content-Type', 'application/json');
    }
    request(args: any) {
        let params = args.params || {};
        args = args || {};
        let url = this.API_URL + (args.url || '');
        let method = args.method || 'GET';
        let body = args.body || {};

        if (this.csrftoken && !this.headers.has('X-CSRFToken')) {
            this.headers.append('X-CSRFToken', this.csrftoken);
        }
        this.options.headers = this.headers;
        this.options.url = url;
        this.options.params = params;
        this.options.method = method;
        this.options.body = body;


        return this.http.request(url, this.options );

    }
    product(limit: number = null, offset: number = null, quantity: number = null, price: number = null, lastUpdate: number = null) {
        let url = '?limit=' + limit + '&offset=' + offset;
        if (quantity) {
            url += '&quantity=' + quantity;
        }
        if (price) {
            url += '&price=' + price;
        }
        if (lastUpdate) {
            url += '&lastUpdate=' + lastUpdate;
        }
        return this.request({
            'method': 'GET',
            'url': url,
        }).map(
            (response: Response) => {
               return response.json();
            }
        );
    }
    validate(upc: string = null) {
        let url = 'check/';

        if (upc) {
            url +=  '?upc=' + upc;
        }
        return this.request({
            'method': 'GET',
            'url': url,
        }).map(
            (response: Response) => {
                return response.json();
            }
        );
    }
    create(upc: string = null) {
        let url = 'create/';

        if (upc) {
            url +=  '?upc=' + upc;
        }
        return this.request({
            'method': 'GET',
            'url': url,
        }).map(
            (response: Response) => {
                return response.json();
            }
        );
    }
    upload(file: File) {
        let formData: FormData = new FormData();
        formData.append('files', file, file.name);
        console.log(formData);
        this.headers.set('Content-Type', 'multipart/form-data');
        this.headers.set('Accept', 'application/json');
        return this.http.post('/api/products/upload', formData, this.options.headers = this.headers).map(
            (response: Response) => {
                console.log(response);
            }
        );
       /* return this.request({
            'method': 'POST',
            'url': 'upload/',
            'body': formData,
        }).map(
            (response: Response) => {
                return response.json();
            }
        );*/
    }
    delete(id: number) {
        let body =  JSON.stringify({
            id: id,
        });
        return this.request({
            'method': 'POST',
            'url': 'delete/',
            'body': body
        }).map(
            (response: Response) => {
                return response.json();
            }
        );
    }
}