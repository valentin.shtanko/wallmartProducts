import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AuthenticationService} from '../services/authentication.service';
import { User } from '../models/user.model';

declare let jQuery: any;
declare let FastClick: any;
declare let __moduleName: string;

@Component({
  moduleId: __moduleName,
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  providers: [AuthenticationService]
})
export class DashboardComponent implements OnInit, AfterViewInit {

  userModel: User;

  constructor(private authenticationService: AuthenticationService) {}
  ngOnInit() {
    this.userModel = new User();
    this.profile();
  }

  ngAfterViewInit() {
      if (jQuery.AdminLTE) {
        jQuery.AdminLTE.layout.activate();
    }
      jQuery('#edit_with_polarr_floater').hide();
  }
   ngOnDestroy() {
  }

  profile() {
     this.authenticationService.profile()
            .subscribe(
                data => {
                  this.userModel = data;
                },
                error => {

                });
  }
}
