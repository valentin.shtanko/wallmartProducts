"use strict";
/* tslint:disable:no-unused-variable */
const testing_1 = require("@angular/core/testing");
const control_sidebar_component_1 = require("./control-sidebar.component");
describe('ControlSidebarComponent', () => {
    let component;
    let fixture;
    beforeEach(testing_1.async(() => {
        testing_1.TestBed.configureTestingModule({
            declarations: [control_sidebar_component_1.ControlSidebarComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = testing_1.TestBed.createComponent(control_sidebar_component_1.ControlSidebarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=control-sidebar.component.spec.js.map