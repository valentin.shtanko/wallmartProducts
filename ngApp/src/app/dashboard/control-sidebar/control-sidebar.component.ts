import { Component, OnInit, AfterViewInit } from '@angular/core';

declare let jQuery: any;
declare let __moduleName: string;
@Component({
  moduleId: __moduleName,
  selector: 'control-sidebar',
  templateUrl: './control-sidebar.component.html'
})
export class ControlSidebarComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    if (jQuery.AdminLTE && jQuery.AdminLTE.controlSidebar) {
      jQuery.AdminLTE.controlSidebar.activate();
    }
  }
}
