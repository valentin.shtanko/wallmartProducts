"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
let ControlSidebarComponent = class ControlSidebarComponent {
    constructor() { }
    ngOnInit() {
    }
    ngAfterViewInit() {
        if (jQuery.AdminLTE && jQuery.AdminLTE.controlSidebar) {
            jQuery.AdminLTE.controlSidebar.activate();
        }
    }
};
ControlSidebarComponent = __decorate([
    core_1.Component({
        selector: 'control-sidebar',
        // templateUrl: './control-sidebar.component.html'
        templateUrl: '/template/dashboard/sidebar'
    }),
    __metadata("design:paramtypes", [])
], ControlSidebarComponent);
exports.ControlSidebarComponent = ControlSidebarComponent;
//# sourceMappingURL=control-sidebar.component.js.map