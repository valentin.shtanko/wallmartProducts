import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// libs
import {NgbModule, NgbModalModule, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MaterialModule, MdToolbarModule, MdCheckboxModule, MdDatepickerModule, MdInputModule, MdNativeDateModule, MdButtonModule, MdTooltipModule, MdSidenavModule } from '@angular/material';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MainSideComponent } from './main-side/main-side.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { FooterComponent } from './footer/footer.component';
import { ControlSidebarComponent } from './control-sidebar/control-sidebar.component';
import { WelcomeComponent } from './content/welcome/welcome.component';
import { ProductsComponent } from './content/products/products.component';

import { AuthGuard } from '../guards/auth.guard';
import { AuthenticationService } from '../services/authentication.service';
import { ProductsService } from '../services/products.service';
import { AddProductModalContent } from './content/products/modals/upload-product.modal';
import { UploadFileModalContent } from './content/products/modals/upload-file.modal';

import { DndComponent } from '../directives/dnd/dnd.component';
import { DndDirective } from '../directives/dnd/dnd.directive';

@NgModule({
    imports: [
        CommonModule,
        NoopAnimationsModule,
        DashboardRoutingModule,
        NgbModule.forRoot(),
        ModalModule.forRoot(),
        NgxDatatableModule,
        FormsModule,
        MaterialModule,
        MdCheckboxModule,
        MdDatepickerModule,
        MdNativeDateModule,
        MdInputModule,
        MdToolbarModule,
        MdButtonModule,
        MdTooltipModule,
        MdSidenavModule
    ],
    declarations: [
        DashboardComponent,
        // Wrap tree
        MainSideComponent,
        MainHeaderComponent,
        FooterComponent,
        ControlSidebarComponent,
        // Content pages
        WelcomeComponent,
        ProductsComponent,
        // modals
        AddProductModalContent,
        UploadFileModalContent,
        // directives
        DndComponent,
        DndDirective
    ],
    providers: [ AuthenticationService, ProductsService, AuthGuard],
    entryComponents: [AddProductModalContent],
    exports: [DashboardComponent]
})
export class DashboardModule { }
