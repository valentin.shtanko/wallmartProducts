import {Component, OnInit, AfterViewInit, ViewChild, TemplateRef} from '@angular/core';
import { DatatableComponent, ColumnMode } from '@swimlane/ngx-datatable';

import {ProductsService} from "../../../services/products.service";

import { AddProductModalContent } from './modals/upload-product.modal';
import { UploadFileModalContent } from './modals/upload-file.modal';
import {MdTooltip} from "@angular/material";

declare let jQuery: any;
declare let __moduleName: string;

@Component({
  moduleId: __moduleName,
  selector: 'products',
  templateUrl: './products.component.html',
  providers: [ProductsService, AddProductModalContent, UploadFileModalContent]
})
export class ProductsComponent implements OnInit, AfterViewInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('imgTmpl') imgTmpl: TemplateRef<any>;
  @ViewChild('fShippingTmpl') fShippingTmpl: TemplateRef<any>;
  @ViewChild('stockTmpl') stockTmpl: TemplateRef<any>;
  @ViewChild('dateTmpl') dateTmpl: TemplateRef<any>;
  @ViewChild('actionTmpl') actionTmpl: TemplateRef<any>;
  @ViewChild('mdTooltip') mdTooltip: MdTooltip;
  @ViewChild('mdTooltipRef') mdTooltipRef: any;

  public filter: any = {
  };
  rows: any = [];
  offset: number = 0;
  limit: number = 10;
  data: any [];
  position = 'before';
  smallImage: boolean = true;
  deleteLoading: boolean = false;
  constructor(private productService: ProductsService) {}

  ngOnInit() {
    this.dataTableInit();
    this.filter = {
      active: false,
      quantity: null,
      price: null,
      lastUpdate: null
    };
    // this.mdTooltip.message = this.mdTooltipRef;
  }
  ngAfterViewInit() {
    if (jQuery.AdminLTE) {
        jQuery.AdminLTE.layout.activate();
    }
  }
  ngOnDestroy() {
  }

  dataTableInit() {

    this.getProduct(this.limit, this.offset);
    this.table.rows = [];
    this.table.columns = [
      {
        prop: 'image_url',
        name: 'Image',
        cellTemplate: this.imgTmpl,
        cellClass: 'product-img',
        canAutoResize: true,
        maxWidth: 70,
      },
      { name: 'UPC',
        maxWidth: 100,
      },
      {
        name: 'Name',
        minWidth: 100,
        canAutoResize: true },
      {
        name: 'Brand',
        maxWidth: 100,
        canAutoResize: true
      },
      {
        name: 'Model',
        minWidth: 100,
        maxWidth: 150,
         canAutoResize: true
      },
      {
        name: 'Price',
        maxWidth: 70,
        canAutoResize: true
      },
      {
        name: 'Shipping',
        cellTemplate: this.fShippingTmpl,
        cellClass: 'text-center',
        prop: 'free_shipping',
        maxWidth: 100,
        minWidth: 50,
        canAutoResize: true,
      },
      {
        name: 'In stock',
        prop: 'in_stock',
        cellTemplate: this.stockTmpl,
        cellClass: 'text-center',
        maxWidth: 150,
        minWidth: 100,
        canAutoResize: true,
      },
      {
        name: 'Last update',
        prop: 'last_update',
        maxWidth: 150,
        cellTemplate: this.dateTmpl,
        canAutoResize: true,
      },
      {
        name: '',
        maxWidth: 150,
        cellTemplate: this.actionTmpl,
        canAutoResize: true
      }
    ];
    this.table.rowHeight = 40;
    this.table.columnMode = ColumnMode.force;
    this.table.cssClasses = {
      sortAscending: 'datatable-icon-down',
      sortDescending: 'datatable-icon-up',
      pagerLeftArrow: 'datatable-icon-left',
      pagerRightArrow: 'datatable-icon-right',
      pagerPrevious: 'datatable-icon-prev',
      pagerNext: 'datatable-icon-skip'
    };
    this.table.limit = this.limit;
    this.table.offset = this.offset;
    this.table.externalPaging = true;
    this.table.footerHeight = 40;
  }
  getProduct(limit: number = null, offset: number = null) {
    this.table.loadingIndicator = true;

    this.productService.product(limit, offset, this.filter.quantity, this.filter.price, this.filter.lastUpdate).subscribe(
                data => {
                  this.table.rows = [];
                  for (let product of data['aaData']){
                    this.table.rows.push(product);
                  }
                  this.table.count = data['iTotalRecords'];
                  this.table.loadingIndicator = false;
                  this.table.recalculateColumns();
                },
                error => {

                });
  }

  draw() {
    setTimeout( () => this.table.recalculate(), 1 ) ;
  }

  onPage(event: any) {
    console.log('Page Event', event);
    this.offset = event.offset *  event.limit;
    this.limit = event.limit;
    this.getProduct(this.limit, this.offset);
  }
  search() {
    this.getProduct(this.table.limit, this.offset);
  }
  delete(id: number) {
    this.deleteLoading = true;
    this.productService.delete(id).subscribe(
        data => {
          this.deleteLoading = false;
          this.search();
        },
        error => {
          this.deleteLoading = false;
        }
    );
  }

}
