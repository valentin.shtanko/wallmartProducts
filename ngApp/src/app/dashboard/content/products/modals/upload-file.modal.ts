import {Component, Input, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {ProductsService} from "../../../../services/products.service";

declare let __moduleName: string;

@Component({
    moduleId: __moduleName,
    selector: 'upload-file',
    templateUrl: './upload-file.modal.html',
    encapsulation: ViewEncapsulation.None,
})

export class UploadFileModalContent {
    @ViewChild('childModal') public childModal: ModalDirective;
    @Input() counter: number = 0;
    files: any = [];
    constructor(private productService: ProductsService) {}

    public showChildModal(): void {
        this.childModal.show();
    }

    public hideChildModal(): void {
        this.childModal.hide();
        console.log(this.files);
    }

    upload() {
        if (this.files) {
            for (let file of this.files) {
                this.productService.upload(file).subscribe(
                    data => {},
                    error => {}
                )
            }
        }
    }
}
