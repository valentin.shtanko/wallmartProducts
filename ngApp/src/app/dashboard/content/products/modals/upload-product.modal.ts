/**
 * Created by nkras on 6/14/17.
 */
import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";
import {ProductsService} from "../../../../services/products.service";
import {DatatableComponent} from "@swimlane/ngx-datatable";

// import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
declare let __moduleName: string;
@Component({
    moduleId: __moduleName,
    selector: 'add-product',
    templateUrl: './upload-product.modal.html',
})
export class AddProductModalContent implements OnInit {
    @Output() sync = new EventEmitter();

    @ViewChild('childModal') public childModal: ModalDirective;
    // public product: Product;
    upc: string;
    id: number;
    product: any = {};
    loading: boolean = false;
    loading_create: boolean = false;
    constructor(private productService: ProductsService) {
        this.product = {
            name: '',
            brand: '',
            model: '',
            imgUrl: '',
            src: ''
        };
    }
    ngOnInit() {}
    public showChildModal(): void {
        this.product = {};
        this.childModal.show();
    }

    public validate(): void {
        this.loading = true;
        this.product = {};
        if (this.upc) {
            this.productService.validate(this.upc).subscribe(
                response => {
                    if (response) {
                        this.product.name = response.name;
                        this.product.brand = response.brand;
                        this.product.model = response.model;
                        this.product.imgUrl = response.imgUrl;
                        this.product.src = response.productUrl;
                        this.loading = false;
                        console.log(response);
                    }
                },
                error => {
                    console.log(error);
                    this.loading = false;
                });
        } else {
            this.loading = false;
        }
    }
    public create(): void {
        this.loading_create = true;
        if (this.upc) {
            this.productService.create(this.upc).subscribe(
                response => {
                    if (response) {
                        this.loading_create = false;
                         this.sync.emit();
                        console.log(response);
                    }
                },
                error => {
                    console.log(error);
                    this.loading_create = false;
                });
        } else {
            this.loading_create = false;
        }
    }
    public hideChildModal(): void {

        this.childModal.hide();
    }
}
