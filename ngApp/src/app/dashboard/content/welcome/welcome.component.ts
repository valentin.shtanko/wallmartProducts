import { Component, OnInit } from '@angular/core';

declare let __moduleName: string;

@Component({
  moduleId: __moduleName,
  selector: 'welcome',
  templateUrl: './welcome.component.html'
})
export class WelcomeComponent implements OnInit {

  ngOnInit() {
  }

   ngOnDestroy() {
  }

}
