import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { WelcomeComponent } from './content/welcome/welcome.component';
import { ProductsComponent } from './content/products/products.component';

import { AuthGuard } from '../guards/auth.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'dashboard',
        component: DashboardComponent,
        children: [
            {
                path: '',
                component: WelcomeComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'products',
                component: ProductsComponent,
                canActivate: [AuthGuard],
            }
        ]
     }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule { }
