import { Component, OnInit } from '@angular/core';

declare let __moduleName: string;

@Component({
  moduleId: __moduleName,
  selector: 'main-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
