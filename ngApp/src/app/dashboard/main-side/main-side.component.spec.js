"use strict";
/* tslint:disable:no-unused-variable */
const testing_1 = require("@angular/core/testing");
const main_side_component_1 = require("./main-side.component");
describe('MainSideComponent', () => {
    let component;
    let fixture;
    beforeEach(testing_1.async(() => {
        testing_1.TestBed.configureTestingModule({
            declarations: [main_side_component_1.MainSideComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = testing_1.TestBed.createComponent(main_side_component_1.MainSideComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=main-side.component.spec.js.map