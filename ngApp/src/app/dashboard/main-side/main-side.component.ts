import {Component, Input, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {User} from '../../models/user.model';

declare let __moduleName: string;

@Component({
    moduleId: __moduleName,
    selector: 'main-side',
    templateUrl: './main-side.component.html',
    providers: [AuthenticationService]
})
export class MainSideComponent implements OnInit {

  @Input() model: User;

  constructor( ) {
      // this.model.firstName = 'Max';
  }

  ngOnInit() {
    this.model.firstName = 'Max';
  }

}
