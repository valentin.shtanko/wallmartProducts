"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const core_1 = require("@angular/core");
const common_1 = require("@angular/common");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const dashboard_routing_module_1 = require("./dashboard-routing.module");
const dashboard_component_1 = require("./dashboard.component");
const main_side_component_1 = require("./main-side/main-side.component");
const main_header_component_1 = require("./main-header/main-header.component");
const footer_component_1 = require("./footer/footer.component");
const control_sidebar_component_1 = require("./control-sidebar/control-sidebar.component");
const welcome_component_1 = require("./content/welcome/welcome.component");
let DashboardModule = class DashboardModule {
};
DashboardModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            dashboard_routing_module_1.DashboardRoutingModule,
            ng_bootstrap_1.NgbModule
        ],
        declarations: [
            dashboard_component_1.DashboardComponent,
            main_side_component_1.MainSideComponent,
            main_header_component_1.MainHeaderComponent,
            footer_component_1.FooterComponent,
            control_sidebar_component_1.ControlSidebarComponent,
            welcome_component_1.WelcomeComponent
        ],
        exports: [dashboard_component_1.DashboardComponent]
    })
], DashboardModule);
exports.DashboardModule = DashboardModule;
//# sourceMappingURL=dashboard.module.js.map