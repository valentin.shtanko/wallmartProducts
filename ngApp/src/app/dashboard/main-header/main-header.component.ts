import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user.model';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';

declare let __moduleName: string;

@Component({
  moduleId: __moduleName,
  selector: 'main-header',
  templateUrl: './main-header.component.html',
})
export class MainHeaderComponent implements OnInit {

  @Input() model: User;

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }
  logout() {
     this.authenticationService.logout().subscribe(
            data => {
              this.router.navigate(['/login']);
            }
        );
    }
}
