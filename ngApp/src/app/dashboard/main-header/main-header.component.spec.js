"use strict";
/* tslint:disable:no-unused-variable */
const testing_1 = require("@angular/core/testing");
const main_header_component_1 = require("./main-header.component");
describe('MainHeaderComponent', () => {
    let component;
    let fixture;
    beforeEach(testing_1.async(() => {
        testing_1.TestBed.configureTestingModule({
            declarations: [main_header_component_1.MainHeaderComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = testing_1.TestBed.createComponent(main_header_component_1.MainHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=main-header.component.spec.js.map