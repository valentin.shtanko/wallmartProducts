/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function(global) {


  // map tells the System loader where to look for things
  var map = {
      // our app is within the app folder
      'app': 'app',

      // angular bundles
      '@angular/animations':                    'npm:@angular/animations/bundles/animations.umd.js',
      '@angular/animations/browser':            'npm:@angular/animations/bundles/animations-browser.umd.js',
      '@angular/core':                          'npm:@angular/core/bundles/core.umd.js',
      '@angular/common':                        'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler':                      'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser':              'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser/animations':   'npm:@angular/platform-browser/bundles/platform-browser-animations.umd.js',
      '@angular/platform-browser-dynamic':      'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http':                          'npm:@angular/http/bundles/http.umd.js',
      '@angular/router':                        'npm:@angular/router/bundles/router.umd.js',
      '@angular/router/upgrade':                'npm:@angular/router/bundles/router-upgrade.umd.js',
      '@angular/forms':                         'npm:@angular/forms/bundles/forms.umd.js',
      '@angular/upgrade':                       'npm:@angular/upgrade/bundles/upgrade.umd.js',
      '@angular/upgrade/static':                'npm:@angular/upgrade/bundles/upgrade-static.umd.js',
      '@angular/material':                      'npm:@angular/material/bundles/material.umd.js',

      'jQuery': 'npm:jquery/dist/jquery.js',
      '@ng-bootstrap/ng-bootstrap':             'npm:@ng-bootstrap/ng-bootstrap/bundles/ng-bootstrap.js',
      'ngx-bootstrap':                          'npm:ngx-bootstrap',
      '@swimlane/ngx-datatable':                'npm:@swimlane/ngx-datatable',
      'rxjs':                                   'npm:rxjs',
      'angular-in-memory-web-api':              'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
      'ngx-cookies':                            'npm:ngx-cookies'
  };

  // packages tells the System loader how to load when no filename and/or no extension
  var packages = {
    'app':                          { main: 'main.js',  defaultExtension: 'js' },
    'rxjs':                         { defaultExtension: 'js' },
    'angular2-in-memory-web-api':   { main: 'index.js', defaultExtension: 'js' },
    'ngx-cookies':                  { main: 'index.ts', defaultExtension: 'ts'},
    '@swimlane/ngx-datatable':      { main: 'release/index.js', defaultExtension: 'js'},
    'ngx-bootstrap':                { main: '/bundles/ngx-bootstrap.umd.js', defaultExtension: 'js'}
  };

  var config = {
    map: map,
    baseURL: '/ng/',
    packages: packages,
    paths: {
      // paths serve as alias
      'npm:': 'node_modules/'
    }
  };

  System.config(config);

})(this);
